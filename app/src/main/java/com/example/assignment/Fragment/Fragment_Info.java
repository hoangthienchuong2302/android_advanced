package com.example.assignment.Fragment;


import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;


import com.example.assignment.DAO.DAO_User;
import com.example.assignment.Database.DbHelper;
import com.example.assignment.R;
import com.example.assignment.model.User;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.single.PermissionListener;

import java.io.ByteArrayOutputStream;

import de.hdodenhof.circleimageview.CircleImageView;



public class Fragment_Info extends Fragment {
    public DAO_User userDAO;
    private static final int GALLER_ACTION_PICK_CODE = 100;
    TextView tvName,tvPhoneNumber,tvAddress,tvUser;
    EditText edtFullName,edtAddress,edtPhoneNumber,edtUser;
    CircleImageView imgUser,imgUserBt;
    DbHelper dbHelper;
    User user;
    Button btnChangeIf,btnChange;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate( R.layout.fragment_info, container, false );
        userDAO = new DAO_User( getContext() );
        tvName = view.findViewById( R.id.tvName );
        tvPhoneNumber = view.findViewById( R.id.tvPhoneNumber );
        tvAddress = view.findViewById( R.id.tvAddress );
        tvUser = view.findViewById( R.id.tvUser );
        btnChangeIf= view.findViewById( R.id.btnChangeIf );
        imgUser= view.findViewById( R.id.imgUser );

        tvUser.setText( dbHelper.Username );

        setData();
        btnChangeIf.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openDialog( getContext() );
            }
        } );
        return view;
    }
    public void openDialog(Context context){
        final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog( context );
        bottomSheetDialog.setContentView( R.layout.bottom_sheet_edit_inf );
        final String userName= dbHelper.Username;
        btnChange = bottomSheetDialog.findViewById( R.id.btnChange );
        edtFullName = bottomSheetDialog.findViewById( R.id.edtFullName );
        edtAddress = bottomSheetDialog.findViewById( R.id.edtAddress );
        edtPhoneNumber = bottomSheetDialog.findViewById( R.id.edtPhoneNumber );
        edtUser= bottomSheetDialog.findViewById( R.id.edtUser );
        imgUserBt=bottomSheetDialog.findViewById( R.id.imgUser );

        imgUserBt.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                runTimePermission();
            }
        } );

        edtPhoneNumber.setText( userDAO.getUser( dbHelper.IDuser ).get( 0 ).getPhoneNumber(  ));
        edtUser.setText( userName );
        edtFullName.setText( userDAO.getUser( dbHelper.IDuser ).get( 0 ).getFullName(  ));
        edtAddress.setText( userDAO.getUser( dbHelper.IDuser ).get( 0 ).getAddress(  ) );
        if( userDAO.getUser(dbHelper.IDuser).get( 0 ).getImages()!=null){
            byte[] imgView= userDAO.getUser(dbHelper.IDuser).get( 0 ).getImages();
            Bitmap bitmap= BitmapFactory.decodeByteArray( imgView,0,imgView.length );
            imgUserBt.setImageBitmap( bitmap );
        }else{
            imgUserBt.setImageResource( R.drawable.user );
        }
        btnChange.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int ID= dbHelper.IDuser;
                String fullName =edtFullName.getText().toString();
                String adress = edtAddress.getText().toString();
                String phoneNumber = edtPhoneNumber.getText().toString();

                if(phoneNumber.length()==10) {
                    user = new User( );
                    user.setAccountID( ID );
                    user.setUsername( userName );
                    user.setFullName( fullName );
                    user.setAddress( adress );
                    user.setPhoneNumber( phoneNumber );
                    user.setImages( imageViewToByte(imgUserBt) );

                    userDAO = new DAO_User( getContext() );
                    userDAO.update( user );
                    Toast.makeText( getContext(), "Đổi thông tin thành công", Toast.LENGTH_SHORT ).show();
                    bottomSheetDialog.dismiss();
                    setData();
                }else{
                    Toast.makeText( getContext(), "Số điện thoại chỉ có 10 số, hãy kiểm tra lại.", Toast.LENGTH_SHORT ).show();
                }
            }
        } );
        bottomSheetDialog.show();
    }
    public void setData(){
        tvName.setText( userDAO.getUser( dbHelper.IDuser ).get( 0 ).getFullName(  ));
        tvPhoneNumber.setText( userDAO.getUser( dbHelper.IDuser ).get( 0 ).getPhoneNumber(  ));
        tvAddress.setText( userDAO.getUser( dbHelper.IDuser ).get( 0 ).getAddress(  ) );
        Bitmap bitmap = null;
        if(userDAO.getUser( dbHelper.IDuser ).get( 0 ).getImages()!=null){
            byte[] imgView= userDAO.getUser( dbHelper.IDuser ).get( 0 ).getImages();
            bitmap= BitmapFactory.decodeByteArray( imgView,0,imgView.length );
            imgUser.setImageBitmap( bitmap );
        }else{
            imgUser.setImageResource( R.drawable.user1 );
        }

    }
    public  void runTimePermission(){
        Dexter.withContext( getContext()).withPermission( Manifest.permission.READ_EXTERNAL_STORAGE).withListener( new PermissionListener() {
            @Override
            public void onPermissionGranted(PermissionGrantedResponse permissionGrantedResponse) {
                galleryIntent();
            }

            @Override
            public void onPermissionDenied(PermissionDeniedResponse permissionDeniedResponse) {

            }

            @Override
            public void onPermissionRationaleShouldBeShown(com.karumi.dexter.listener.PermissionRequest permissionRequest, PermissionToken permissionToken) {
                permissionToken.continuePermissionRequest();
            }
        }).check();
    }
    //Pick Image From Gallery
    private void galleryIntent() {
        Intent i = new Intent(Intent.ACTION_PICK);
        i.setType("image/*");
        startActivityForResult(i,GALLER_ACTION_PICK_CODE);
    }
    //Convert Bitmap To Byte
    public static byte[] imageViewToByte(ImageView image) {
        Bitmap bitmap = ((BitmapDrawable)image.getDrawable()).getBitmap();
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap= getResizedBitmap( bitmap,1024 );
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] byteArray = stream.toByteArray();
        return byteArray;
    }
    public static Bitmap getResizedBitmap(Bitmap bitmap, int maxSize) {
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        float bitmapRatio = (float) width / height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(bitmap, width, height, true);
    }

    //Convert Byte To BitMap
    public static Bitmap convertCompressedByteArrayToBitmap(byte[] src){
        return BitmapFactory.decodeByteArray(src, 0, src.length);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == Activity.RESULT_OK){
            if(requestCode == GALLER_ACTION_PICK_CODE){
                Uri imageUri = data.getData();
                imgUserBt.setImageURI(imageUri);
            }
        }
    }
}
