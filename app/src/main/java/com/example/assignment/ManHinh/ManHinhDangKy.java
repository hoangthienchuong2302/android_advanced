package com.example.assignment.ManHinh;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;


import com.example.assignment.BuildConfig;
import com.example.assignment.DAO.DAO_User;
import com.example.assignment.ManHinh.ManHinhChinhAdmin;
import com.example.assignment.R;
import com.example.assignment.model.User;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.single.PermissionListener;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Random;

import de.hdodenhof.circleimageview.CircleImageView;

public class ManHinhDangKy extends AppCompatActivity {
    private static final int GALLER_ACTION_PICK_CODE = 100;
    EditText edtUser,edtPass,edtFullName,edtAddress,edtPhoneNumber;
    Button btnDangKy;
    DAO_User userDAO;
    User user;
    Uri imageUri;
    Bitmap bitmap;
    private File destination = null;
    private InputStream inputStreamImg;
    private String imgPath = null;
    CircleImageView imageUser;
    int PERMISSION_ALL = 1;

    private final int PICK_IMAGE_CAMERA = 1, PICK_IMAGE_GALLERY = 2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.fragment_signup );

        String[] PERMISSIONS = {
                android.Manifest.permission.READ_EXTERNAL_STORAGE,
                android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                android.Manifest.permission.CAMERA,
                android.Manifest.permission.ACCESS_FINE_LOCATION
        };
        if (!hasPermissions(this, PERMISSIONS)) {
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
        }

        edtUser= findViewById( R.id.edtUser );
        edtPass= findViewById( R.id.edtPass );
        edtFullName= findViewById( R.id.edtFullName );
        edtAddress= findViewById( R.id.edtAddress );
        edtPhoneNumber= findViewById( R.id.edtPhoneNumber );
        imageUser= findViewById( R.id.imgUser );
        btnDangKy=findViewById( R.id.btnDangKy );
        imageUser.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectImage();
            }
        });
        btnDangKy.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                userDAO = new DAO_User( ManHinhDangKy.this );
                int count=0;
                String User = edtUser.getText().toString();
                String Pass = edtPass.getText().toString();
                String FullName = edtFullName.getText().toString();
                String Adress = edtAddress.getText().toString();
                String PhoneNumber = edtPhoneNumber.getText().toString();
                if (!User.equals( "" ) && !Pass.equals( "" ) && !FullName.equals( "" ) && !Adress.equals( "" ) && !PhoneNumber.equals( "" ) && PhoneNumber.length()==10){
                    for(int i= 0;i<userDAO.getUser(  ).size();i++){
                        if(edtUser.getText().toString().equalsIgnoreCase( userDAO.getUser().get( i ).getUsername() )){
                            count++;
                        }
                    }if(count==0) {
                        user = new User();
                        user.setUsername( User );
                        user.setPassword( Pass );
                        user.setFullName( FullName );
                        user.setAddress( Adress );
                        user.setPhoneNumber( PhoneNumber );
                        user.setImages( imageViewToByte(imageUser));
                        userDAO.insert( user );
                        Toast.makeText( ManHinhDangKy.this, "Thêm thành công", Toast.LENGTH_SHORT ).show();
                        Intent intentManHinhLogin = new Intent( getBaseContext(), ManHinhChinhAdmin.class );
                        startActivity( intentManHinhLogin );
                        overridePendingTransition( android.R.anim.slide_out_right,android.R.anim.fade_out);
                    }else{
                        edtUser.setError( "User đã tồn tại, bạn hãy chọn tên mới" );
                    }
                }else if(User.equals( "" ) || Pass.equals( "" ) || FullName.equals( "" ) || Adress.equals( "" ) || PhoneNumber.equals( "" )){
                    if(User.equals( "" )){
                        edtUser.setError( "Bạn chưa nhập User Name" );
                    }
                    if (Pass.equals( "" )){
                        edtPass.setError( "Bạn chưa nhập Password" );
                    }
                    if (FullName.equals( "" )){
                        edtFullName.setError( "Bạn chưa nhập Full Name" );
                    }
                    if (Adress.equals( "" )){
                        edtAddress.setError( "Bạn chưa nhập Address" );
                    }
                    if (PhoneNumber.equals( "" )){
                        edtPhoneNumber.setError( "Bạn chưa nhập Phone Number" );
                    }
                }else if(PhoneNumber.length()!=10){
                    edtPhoneNumber.setError( "Số điện thoại chỉ có 10 số" );
                }
            }
        } );
    }
    public  void runTimePermission(){
        Dexter.withContext(ManHinhDangKy.this).withPermission( Manifest.permission.READ_EXTERNAL_STORAGE).withListener( new PermissionListener() {
            @Override
            public void onPermissionGranted(PermissionGrantedResponse permissionGrantedResponse) {
                galleryIntent();
            }

            @Override
            public void onPermissionDenied(PermissionDeniedResponse permissionDeniedResponse) {

            }

            @Override
            public void onPermissionRationaleShouldBeShown(com.karumi.dexter.listener.PermissionRequest permissionRequest, PermissionToken permissionToken) {
                permissionToken.continuePermissionRequest();
            }
        }).check();
    }
    //Pick Image From Gallery
    private void galleryIntent() {
        Intent i = new Intent(Intent.ACTION_PICK);
        i.setType("image/*");
        startActivityForResult(i,GALLER_ACTION_PICK_CODE);
    }
    //Convert Bitmap To Byte
    public static byte[] imageViewToByte(ImageView image) {
        Bitmap bitmap = ((BitmapDrawable)image.getDrawable()).getBitmap();
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap= getResizedBitmap( bitmap,1024 );
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] byteArray = stream.toByteArray();
        return byteArray;
    }
    public static Bitmap getResizedBitmap(Bitmap bitmap, int maxSize) {
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        float bitmapRatio = (float) width / height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(bitmap, width, height, true);
    }

    //Convert Byte To BitMap
    public static Bitmap convertCompressedByteArrayToBitmap(byte[] src){
        return BitmapFactory.decodeByteArray(src, 0, src.length);
    }


    public void selectImage() {
        try {
            PackageManager pm = this.getPackageManager();
            int hasPerm = pm.checkPermission( Manifest.permission.CAMERA, this.getPackageName());
            if (hasPerm == PackageManager.PERMISSION_GRANTED) {
                final CharSequence[] options = {"Chụp Ảnh", "Thư Viện Ảnh", "Hủy"};
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle( Html.fromHtml("<font color='#172737'>Tùy Chọn</font>"));
                builder.setItems(options, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int item) {
                        if (options[item].equals("Chụp Ảnh")) {
                            dialog.dismiss();
                            Intent intent = new Intent( MediaStore.ACTION_IMAGE_CAPTURE);
                            imageUri = FileProvider.getUriForFile(ManHinhDangKy.this, BuildConfig.APPLICATION_ID + ".provider", getOutputMediaFile());
                            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                            intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);

                            startActivityForResult(intent, PICK_IMAGE_CAMERA);

                        } else if (options[item].equals("Thư Viện Ảnh")) {
                            runTimePermission();
                        } else if (options[item].equals("Hủy")) {
                            dialog.dismiss();
                        }
                    }
                });
                builder.show();
            } else
                Toast.makeText(this, "Camera Permission error", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            Toast.makeText(this, "Camera Permission error", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }
    public static boolean hasPermissions(Context context, String... permissions) {
        if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }
    private static File getOutputMediaFile() {
        File mediaStorageDir = new File( Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "Poly Bookstore");

        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        return new File(mediaStorageDir.getPath() + File.separator +
                "IMG_" + timeStamp + ".jpg");
    }
    public String getRealPathFromURI(Uri contentUri) {
        String[] proj = {MediaStore.Audio.Media.DATA};
        Cursor cursor = this.managedQuery(contentUri, proj, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }
    private Bitmap decodeUri(Uri selectedImage, int REQUIRED_SIZE) {

        try {
            // Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(this.getContentResolver().openInputStream(selectedImage), null, o);

            // The new size we want to scale to
            // final int REQUIRED_SIZE =  size;
            // Find the correct scale value. It should be the power of 2.
            int width_tmp = o.outWidth, height_tmp = o.outHeight;
            int scale = 1;
            while (true) {
                if (width_tmp / 2 < REQUIRED_SIZE || height_tmp / 2 < REQUIRED_SIZE) {
                    break;
                }
                width_tmp /= 2;
                height_tmp /= 2;
                scale *= 2;
            }

            // Decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            return BitmapFactory.decodeStream(this.getContentResolver().openInputStream(selectedImage), null, o2);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        inputStreamImg = null;
        if(resultCode == Activity.RESULT_OK){
            if(requestCode == GALLER_ACTION_PICK_CODE){
                imageUri = data.getData();
                imageUser.setImageURI(imageUri);
            }
        }
        if (requestCode == PICK_IMAGE_CAMERA) {
            try {
                bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), imageUri);
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);

                Log.e("Activity", "Pick from Camera::>>> ");

                String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
                destination = new File(Environment.getExternalStorageDirectory() + "/" +
                        getString(R.string.app_name), "IMG_" + timeStamp + ".jpg");
                FileOutputStream fo;
                try {
                    destination.createNewFile();
                    fo = new FileOutputStream(destination);
                    fo.write(bytes.toByteArray());
                    fo.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                imgPath = destination.getAbsolutePath();
                imageUser.setImageBitmap(bitmap);
                saveImageToExternalStorage(bitmap);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    private void saveImageToExternalStorage(Bitmap finalBitmap) {
        String root = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).toString();
        File myDir = new File(root + "/saved_images");
        myDir.mkdirs();
        Random generator = new Random();
        int n = 10000;
        n = generator.nextInt(n);
        String fname = "Image-" + n + ".jpg";
        File file = new File(myDir, fname);
        if (file.exists())
            file.delete();
        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }


        // Tell the media scanner about the new file so that it is
        // immediately available to the user.
        MediaScannerConnection.scanFile(this, new String[]{file.toString()}, null,
                new MediaScannerConnection.OnScanCompletedListener() {
                    public void onScanCompleted(String path, Uri uri) {
                        Log.i("ExternalStorage", "Scanned " + path + ":");
                        Log.i("ExternalStorage", "-> uri=" + uri);
                    }
                });

    }
}
